package org.itstep.qa.homework;

import java.sql.*;

public class SQLRunner {
    public static void main(String[] args) {
        Connection connection = null;
        Statement stat = null;

        // Запрос базовый
        String query = "select * from park";

        // Заполнение таблицы с типами аттракциона
        String queryUpdateType = " INSERT into ride_type values (1,'moving')";
        String queryUpdateTyp = " INSERT into ride_type values (2,'static')";

        // Заполнение таблицы park
        String queryUpdatePark1 = "insert into park values (1,'Scary Castle', '100', '2')";
        String queryUpdatePark2 = "insert into park values (2,'Merry-go-round', '30', '1')";
        String queryUpdatePark3 = "insert into park values (3,'Ferris Tower', '50', '2')";
        String queryUpdatePark4 = "insert into park values (4,'Rollercoaster', '33', '1')";

        String deleteValuesFromPark ="delete from park;";
        String deleteValuesFromRideType ="delete from ride_type;";


        // Запросы по заданию

        // Количество аттракционов
        String queryAttractionNumber= "select count(park.id) from park;";

        // Только передвижные аттракционы
        String queryMovingAttraction= "select attr_name from park where type_id=1";

        // Аттракционы, количество посадочных мест у которых больше 33
        String queryMore33Seats="select attr_name from park where seat_num >33;";

        // Суммарное количество мест у передвижных и стационарных аттракционов
        String queryAllSeats="select  sum(seat_num) from park;";



        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/lunapark", "root", "root");
            stat = connection.createStatement();
         stat.executeUpdate(queryUpdateType);
            stat.executeUpdate(queryUpdateTyp);
            stat.executeUpdate(queryUpdatePark1);
            stat.executeUpdate(queryUpdatePark2);
            stat.executeUpdate(queryUpdatePark3);
            stat.executeUpdate(queryUpdatePark4);



            // Простой вывод содержимого таблицы парк
            ResultSet rs = stat.executeQuery(query);
            while (rs.next()) {
                System.out.println(rs.getString("id")
                        + "     " +
                        rs.getString("attr_name")
                        + "     " +
                        rs.getString("seat_num")
                        + "     " +
                        rs.getString("type_id")
                );
            }

            System.out.println();

            // Количество аттракционов
            ResultSet rs1 = stat.executeQuery(queryAttractionNumber);
            while (rs1.next()) {
                System.out.println("Количество аттракционов");
                System.out.println(rs1.getString("count(park.id)")
                );
            }

            System.out.println();

            // Только передвижные аттракционы
            ResultSet rs2 = stat.executeQuery(queryMovingAttraction);
            System.out.println("Только передвижные аттракционы");
            while (rs2.next()) {
                System.out.println(rs2.getString("attr_name")
                );
            }

            System.out.println();

            // Аттракционы, количество посадочных мест у которых больше 33
            ResultSet rs3 = stat.executeQuery(queryMore33Seats);
            System.out.println("Аттракционы, количество посадочных мест у которых больше 33");
            while (rs3.next()) {
                System.out.println(rs3.getString("attr_name")

                );
            }

            System.out.println();

            // Суммарное количество мест у передвижных и стационарных аттракционов
            ResultSet rs4 = stat.executeQuery(queryAllSeats);
            System.out.println("Суммарное количество мест у передвижных и стационарных аттракционов");
            while (rs4.next()) {
                System.out.println(rs4.getString("sum(seat_num)")
                );
            }



            // Очищаем таблицы после вывода запросов (это друг подсказал)
            stat.executeUpdate(deleteValuesFromPark);
            stat.executeUpdate(deleteValuesFromRideType);


            // Закрываем подключения
            rs.close();
            rs1.close();
            rs2.close();
            rs3.close();
            rs4.close();
            stat.close();
            connection.close();
        } catch (SQLException exe) {
            System.out.println("Error");
            exe.printStackTrace();
        }
    }
}